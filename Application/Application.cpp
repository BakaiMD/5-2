

#include "stdafx.h"
#include "../Library/PetriNet.h"
#include <iostream>
#include <ctime>

template <class number>
int getNum(number &r) {
	std::cin >> r;
	if (!std::cin.good())
		return 1;
	return 0;
}

char *getStr(std::istream &s);

int choice(int&ch, PetriNet& net);

int addArc(PetriNet&net);
int addPlace(PetriNet&net);
int addTransition(PetriNet&net);
int LinkNodes(PetriNet&net);
int Process(PetriNet&net);

int(*f[])(PetriNet&) = { NULL, addArc, addPlace, addTransition, LinkNodes,Process };
const char*msgs[] = { "0. Quit", "1. Add arc", "2. Add place", "3. Add transition", "4. Link nodes","5. Process" };
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

int main()
{
	PetriNet net;
	int ch, err;
	srand(time(NULL));
	do {
		err = choice(ch, net);
		if (err) {
			system("pause");
			return 0;
		}
	} while (ch);
	return 0;
}

int choice(int&ch, PetriNet& net) {
	int err = 0;
	std::cout << std::endl;
	for (int i = 0; i < NMsgs; i++) {
		std::cout << msgs[i] << std::endl;
	}
	do {
		if (getNum(ch)) {
			std::cout << "Incorrect number" << std::endl;
			system("pause");
			return 1;
		}
		if (ch < 0 || ch >= NMsgs) {
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (ch < 0 || ch >= NMsgs);
	if (ch) {
		err = f[ch](net);
		if (err)
			return err;
	}
	else
		err = 0;
	return err;
}

int addArc(PetriNet&net)
{
	Arc* A;
	char* name, *plname;
	std::cout << "Input name: ";
	try {
		name = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	try
	{
		A = new Arc(name);
	}
	catch (std::bad_alloc& ba)
	{
		delete name;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	std::cout << "Input place name: ";
	try {
		plname = getStr(std::cin);
	}
	catch (char*& what) {
		delete A;
		std::cout << what << std::endl;
		return 1;
	}
	try {
		net.addArc(*A, plname);
	}
	catch (std::bad_alloc& ba) {
		delete A;
		delete plname;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	catch (char*&what) {
		delete A;
		delete plname;
		std::cout << what << std::endl;
		return 0;
	}
	delete A;
	delete plname;
	std::cout << "Success" << std::endl;
	return 0;
}

int addPlace(PetriNet&net)
{
	Place* P;
	char* name;
	std::cout << "Input name: ";
	try {
		name = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	try
	{
		P = new Place(name);
	}
	catch (std::bad_alloc& ba)
	{
		delete name;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	try {
		net.addNode(P);
	}
	catch (std::bad_alloc& ba) {
		delete P;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	catch (char*&what) {
		delete P;
		std::cout << what << std::endl;
		return 0;
	}
	std::cout << "Success" << std::endl;
	return 0;
}

int addTransition(PetriNet&net)
{
	Transition* T;
	char* name;
	std::cout << "Input name: ";
	try {
		name = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	try
	{
		T = new Transition(name);
	}
	catch (std::bad_alloc& ba)
	{
		delete name;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	int Time;
	std::cout << "Input delay" << std::endl;
	do {
		if (getNum(Time)) {
			std::cout << "Incorrect number" << std::endl;
			delete name;
			delete T;
			return 1;
		}
		if (Time < 0) {
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (Time < 0);
	T->set(Time);
	try {
		net.addNode(T);
	}
	catch (std::bad_alloc& ba) {
		delete T;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 1;
	}
	catch (char*&what) {
		delete T;
		std::cout << what << std::endl;
		return 0;
	}

	std::cout << "Success" << std::endl;
	return 0;
}

int LinkNodes(PetriNet&net)
{
	char* from, *to;
	std::cout << "Input origin: ";
	try {
		from = getStr(std::cin);
	}
	catch (char*& what) {
		std::cout << what << std::endl;
		return 1;
	}
	std::cout << "Input destination: ";
	try {
		to = getStr(std::cin);
	}
	catch (char*& what) {
		delete from;
		std::cout << what << std::endl;
		return 1;
	}
	try {
		net.linkNodes(from, to);
	}
	catch (std::bad_alloc&ba)
	{
		delete from;
		delete to;
		std::cout << "Failed to allocate memory: " << ba.what() << std::endl;
		return 0;
	}
	catch (char*&what) {
		delete from;
		delete to;
		std::cout << what << std::endl;
		return 0;
	}
	delete from;
	delete to;
	std::cout << "Success" << std::endl;
	return 0;
}

int Process(PetriNet&net)
{
	int Time;
	std::cout << "Input time" << std::endl;
	do {
		if (getNum(Time)) {
			std::cout << "Incorrect number" << std::endl;
			return 1;
		}
		if (Time < 0) {
			std::cout << "Incorrect number, input again" << std::endl;
		}
	} while (Time < 0);
	net.process(Time);
	return 0;
}

char *getStr(std::istream &s)
{
	char buf[21];
	int n, len = 0, curLen;
	char *ptr = new char, *bptr;
	*ptr = '\0';
	s.get();
	do {
		s.get(buf, 21);
		if (s.bad()) {
			delete ptr;
			ptr = nullptr;
			s.clear();
			s.ignore(INT_MAX, '\n');
			throw "Failed to get string";
		}
		if (s.good()) {
			curLen = strlen(buf);
			len += curLen;
			bptr = ptr;
			try {
				ptr = new char[len + 1];
			}
			catch (std::bad_alloc &ba) {
				s.clear();
				s.ignore(INT_MAX, '\n');
				delete bptr;
				throw "Failed to allocate memory for string";
			}
			*ptr = '\0';
			strcat_s(ptr, len + 1, bptr);
			strcat_s(ptr, len + 1, buf);
			delete bptr;
		}
		else
			s.get();
	} while (s.good());
	s.clear();

	return ptr;
}

