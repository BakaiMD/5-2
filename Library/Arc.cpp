#include "stdafx.h"
#include "Arc.h"


Arc::Arc(const char* N) :Pl(nullptr), Tr(nullptr), T(0)
{
	int n;
	n = strlen(N);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), N);
}


Arc::~Arc()
{
	delete Name;
}

void Arc::FindTr()
{
	int n, r;
	n = Pl->getNodeNum();
	if (n == 0) {
		Tr = nullptr;
	}
	else
	{
		r = rand() % n;
		Tr = (Transition*)(Pl->getNode(r));
	}
}

int Arc::tick()
{
	if (Tr == nullptr || Tr->getNodeNum() == 0)
		return 1;
	if (T == Tr->getTime())
	{
		T = 0;
		move();
		return 0;
	}
	T++;
	return 0;
}

void Arc::move()
{
	int n = Tr->getNodeNum(), r;
	r = rand() % n;
	Pl = (Place*)(Tr->getNode(r));
	FindTr();
}

const char* Arc::getName() const
{
	return Name;
}

void Arc::setPlace(Place* pl)
{
	Pl = pl;
	T = 0;
	FindTr();
}

bool Arc::hasPlace() const
{
	if (Pl)
		return true;
	return false;
}

Arc::Arc(const Arc& A) :T(A.T), Pl(A.Pl), Tr(A.Tr)
{
	int n;
	n = strlen(A.Name);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), A.Name);
}

const Arc& Arc::operator = (const Arc& A)
{
	int n;
	n = strlen(A.Name);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), A.Name);
	T = A.T;
	Pl = A.Pl;
	Tr = A.Tr;
	return *this;
}

void Arc::start(const int Time, TiXmlDocument* Log)
{
	TiXmlElement* E, *ME;
	if (Tr == nullptr)
		FindTr();
	ME = new TiXmlElement("Arc");
	ME->SetAttribute("Name", Name);
	Log->LinkEndChild(ME);
	int i, r = 0;
	for (i = 0; (i < Time) && !r; i++)
	{
		E = new TiXmlElement("Note");
		E->SetAttribute("Time", i);
		E->SetAttribute("Place", Pl->getName());
		if (Tr != nullptr)
			E->SetAttribute("Transition", Tr->getName());
		else
			E->SetAttribute("Transition", "No transition");
		E->SetAttribute("Weight", T);
		ME->LinkEndChild(E);
		r = tick();
	}
	if (Tr != nullptr) {
		E = new TiXmlElement("Note");
		E->SetAttribute("Time", i);
		E->SetAttribute("Place", Pl->getName());
		E->SetAttribute("Transition", Tr->getName());
		E->SetAttribute("Weight", T);
		ME->LinkEndChild(E);
	}
}
