#include "stdafx.h"
#include "PetriNet.h"

PetriNet::PetriNet() :NodeTab(), ArcTab()
{
}


PetriNet::~PetriNet()
{
	for (std::vector<Node*>::iterator i = NodeTab.begin(); i != NodeTab.end(); i++)
	{
		switch ((*i)->getType())
		{
		case 1:
			delete (Place*)(*i);
			break;
		case 2:
			delete (Transition*)(*i);
			break;
		default:
			delete *i;
			break;
		}
	}
}

void PetriNet::addArc(Arc A, const char* PlaceName)
{
	Node* N;
	for (std::vector<Arc>::const_iterator i = ArcTab.cbegin(); i != ArcTab.cend(); i++)
	{
		if (strcmp(A.getName(), i->getName()) == 0)
			throw "Name already used";
	}
	N = getNode(PlaceName);
	if (N == nullptr)
		throw "No node found";
	if (N->getType() != 1)
		throw "Arcs can be added only to places";
	A.setPlace((Place*)N);
	ArcTab.push_back(A);
}

void PetriNet::addNode(Node* N)
{
	for (std::vector<Node*>::const_iterator i = NodeTab.cbegin(); i != NodeTab.cend(); i++)
	{
		if (strcmp(N->getName(), (*i)->getName()) == 0)
			throw "Name already used";
	}
	NodeTab.push_back(N);
}

Node* PetriNet::getNode(const char* N)
{
	for (std::vector<Node*>::const_iterator i = NodeTab.cbegin(); i != NodeTab.cend(); i++)
	{
		if (strcmp(N, (*i)->getName()) == 0)
		{
			return *i;
		}
	}
	return nullptr;
}

void PetriNet::linkNodes(const char* from, const char* to)
{
	Node* N1, *N2;
	N1 = getNode(from);
	if (N1 == nullptr)
		throw "Can't find origin";
	if (N1->linked(to))
		throw "Nodes already linked";
	N2 = getNode(to);
	if (N2 == nullptr)
		throw "Can't find destination";
	if (N1->getType() == N2->getType())
		throw "Nodes must have different types";
	N1->addNode(N2);
}

void start(Arc* A, const int T, TiXmlDocument* Log)
{
	srand(((int)A + time(NULL)) % INT_MAX);
	A->start(T, Log);
}

void syncLog(TiXmlDocument* to, TiXmlElement* from)
{
	TiXmlElement* E1, *Ef;
	Ef = from->FirstChildElement("Note");
	for (TiXmlElement* E = to->FirstChildElement("Time_note"); E; E = E->NextSiblingElement("Time_note"))
	{
		E1 = new TiXmlElement("Note");
		E1->SetAttribute("Name", from->Attribute("Name"));
		E1->SetAttribute("Place", Ef->Attribute("Place"));
		E1->SetAttribute("Transition", Ef->Attribute("Transition"));
		E1->SetAttribute("Weight", Ef->Attribute("Weight"));
		if (Ef->NextSiblingElement("Note"))
		{
			E1->SetAttribute("Completed", "No");
			Ef = Ef->NextSiblingElement("Note");
		}
		else
			E1->SetAttribute("Completed", "Yes");
		E->LinkEndChild(E1);
	}
}

void PetriNet::process(const int Time)
{
	TiXmlDeclaration* D = new TiXmlDeclaration("1.0", "", "");
	TiXmlDeclaration* UD = new TiXmlDeclaration("1.0", "", "");
	int   size;
	const int threadsnum = 5;
	TiXmlDocument Log, ULog;
	Log.LinkEndChild(D);
	ULog.LinkEndChild(UD);
	std::thread thread_array[threadsnum];
	size = ArcTab.size();
	for (std::vector<Arc>::iterator i = ArcTab.begin(); i != ArcTab.end();) {
		for (int j = 0; (i != ArcTab.end()) && (j < threadsnum); i++, j++)
			thread_array[j] = std::thread(start, &(*i), Time, &Log);
		for (int j = 0; j < threadsnum; j++) {
			if (thread_array[j].joinable()) {
				thread_array[j].join();
			}
		}
	}
	Log.SaveFile("Log.xml");
	TiXmlElement* E;
	for (int i = 0; i < Time; i++)
	{
		E = new TiXmlElement("Time_note");
		E->SetAttribute("Time", i);
		ULog.LinkEndChild(E);
	}
	for (E = Log.FirstChildElement("Arc"); E; E = E->NextSiblingElement("Arc"))
	{
		syncLog(&ULog, E);
	}
	ULog.SaveFile("ULog.xml");
}