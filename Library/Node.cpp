#include "stdafx.h"
#include "Node.h"

Node::Node() : type(0), Name(nullptr)
{
}

Node::Node(const int Type, const char* N)
{
	int n;
	if (Type > 2 || Type < 0)
		throw "Illegal type";
	type = Type;
	n = strlen(N);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), N);
}


Node::~Node()
{
	Tab.clear();
	delete Name;
}

void Node::addNode(Node* Element)
{
	if (Element->getType() == type || Element->getType() == 0)
		throw "Illegal element";
	Tab.push_back(Element);
}

int Node::getType() const
{
	return type;
}

int Node::getNodeNum() const
{
	int n = 0;
	for (std::vector<Node*>::const_iterator i = Tab.cbegin(); i != Tab.cend(); i++)
	{
		n++;
	}
	return n;
}

Node* Node::getNode(const int num) const
{
	int n = 0;
	for (std::vector<Node*>::const_iterator i = Tab.cbegin(); i != Tab.cend(); i++)
	{
		if (n == num)
			return *i;
		n++;
	}
	return nullptr;
}

const char* Node::getName() const
{
	return Name;
}


Node::Node(const Node& N) :Tab(N.Tab), type(N.type)
{
	int n;
	n = strlen(N.Name);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), N.Name);
}

const Node& Node::operator = (const Node& N)
{
	int n;
	n = strlen(N.Name);
	Name = new char[n + 1];
	Name[0] = '\0';
	strcpy_s(Name, sizeof(char)*(n + 1), N.Name);
	Tab = N.Tab;
	type = N.type;
	return *this;
}

const bool Node::linked(const char* name) const
{
	for (std::vector<Node*>::const_iterator i = Tab.cbegin(); i != Tab.cend(); i++)
	{
		if (strcmp((*i)->getName(), name) == 0)
			return true;
	}
	return false;
}