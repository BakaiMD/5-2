#pragma once
#include <vector>
#include <string>
#include "C:/Users/Bakai/Documents/Visual Studio 2015/Projects/tinyxml/tinyxml.h"


class Node
{
	int type;
	char* Name;
	std::vector<Node*> Tab;
public:
	Node();
	Node(const int type, const char* N);
	Node(const Node& N);
	const Node& operator = (const Node& N);
	void addNode(Node* Element);
	int getType() const;
	int getNodeNum() const;
	const bool linked(const char* name) const;
	Node* getNode(const int num) const;
	const char* getName() const;
	~Node();
};
