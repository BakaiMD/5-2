#include "stdafx.h"
#include "Transition.h"


Transition::Transition(const char* N) :T(0), Node(2, N)
{
}


Transition::~Transition()
{
}

void Transition::set(const int Time)
{
	if (Time > 0)
		T = Time;
}

void Transition::addNode(Node* Place)
{
	if (Place->getType() != 1)
		throw "Illegal node";
	Node::addNode(Place);
}

int Transition::getTime() const
{
	return T;
}