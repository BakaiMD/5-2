#pragma once
#include "Place.h"
#include "Transition.h"
class Arc
{
	int T;
	Place* Pl;
	Transition* Tr;
	char* Name;
	void FindTr();
	void move();
public:
	void start(const int time, TiXmlDocument* Log);
	Arc(const char* N);
	Arc(const Arc& A);
	const Arc& operator = (const Arc& A);
	~Arc();
	const char* getName() const;
	void setPlace(Place*);
	bool hasPlace() const;
	int tick();
};