#pragma once
#include "Node.h"
class Transition : public Node
{
	int T;
public:
	Transition(const char* N);
	~Transition();
	void set(const int Time);
	void addNode(Node* Place);
	int getTime() const;
};