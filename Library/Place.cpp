#include "stdafx.h"
#include "Place.h"


Place::Place(const char* N) :Node(1, N)
{
}


Place::~Place()
{
}

void Place::addNode(Node* Transition)
{
	if (Transition->getType() != 2)
		throw "Illegal node";
	Node::addNode(Transition);
}
