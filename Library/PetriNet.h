#pragma once
#include "Arc.h"
#include <thread>

class PetriNet
{
	std::vector<Node*> NodeTab;
	std::vector<Arc> ArcTab;
public:
	void addArc(Arc, const char* PlaceName);
	void addNode(Node*);
	void linkNodes(const char* from, const char* to);
	void process(const int Time);
	Node* getNode(const char* N);
	PetriNet();
	~PetriNet();
};
